require "test/unit"
require "regurgitator"
require "tempfile"

class TestListKeys < Test::Unit::TestCase
  include Regurgitator::ListKeys
  def setup
    @tmp = Tempfile.new(%w(regurgitator_list_keys .sqlite3))
    rv = system("sqlite3 #{@tmp.path} < t/fixtures.sql")
    assert rv, $?.inspect
    @db = Sequel.connect("sqlite://#{@tmp.path}")
    domain_init
  end

  def test_list_keys
    a = list_keys("d", {})
    assert_kind_of Array, a
    row = a[0]
    assert_equal "blah", row[:dkey]
    [ :fid, :length, :devcount, :classid ].each do |key|
      assert_kind_of Integer, row[key]
    end

    list_keys("d", {:prefix => "bla"})
    assert_equal row, a[0]
    assert_empty list_keys("d", {:prefix => "blahh"})
    assert_empty list_keys("d", {:prefix => "bla", :after => "blah"})

    assert_nil list_keys("afadf")
    list_keys("d") { |r| assert_equal row, r }
  end

  def test_list_keys_insensitive
    a = list_keys("d", :prefix => "B")
    assert a.empty?, a.inspect << " not empty"
    a = list_keys("d", :prefix => "b")
    assert_equal 1, a.size
    assert_equal "blah", a[0][:dkey]
  end
end
