require "test/unit"
require "tmpdir"
require "fileutils"
require "regurgitator"
require "regurgitator/local"

class TestLocal < Test::Unit::TestCase
  include Regurgitator::Local

  def teardown
    STORE_PATHS.clear
    FileUtils.rm_rf @tmpdir
    Regurgitator::Local.refresh_addrs!
  end

  def setup
    @local_addrs = Regurgitator::Local.instance_variable_get(:@local_addrs)
    @local_addrs["127.0.0.1"] = true if @local_addrs.empty?
    @addr = @local_addrs.keys.first
    @tmpdir = Dir.mktmpdir
  end

  def test_device_path_stat
    port = 1234
    uri = URI("http://#@addr:#{port}/dev10/0/004/689/0004689631.fid")
    assert_nil device_path_stat(uri)

    assert_raises(ArgumentError) {
      Regurgitator::Local.register port, @tmpdir
    }
    Dir.mkdir "#@tmpdir/dev10"
    devs = Regurgitator::Local.register port, @tmpdir
    assert_equal [ "dev10" ], devs.to_a

    Regurgitator::Local.register port, @tmpdir
    expect = "#@tmpdir/dev10/0/004/689/0004689631.fid"
    FileUtils.mkdir_p(File.dirname(expect))

    File.open(expect, "wb") { |fp| fp.write '.' }
    assert_equal expect, device_path_stat(uri)[0]
  end

  def test_device_path_stat_ambiguous
    Dir.mkdir(@a = "#@tmpdir/a")
    Dir.mkdir(@b = "#@tmpdir/b")
    port = 1234
    uri = URI("http://#@addr:#{port}/dev10/0/004/689/0004689631.fid")
    assert_nil device_path_stat(uri)

    apath = "#@a/dev10/0/004/689/0004689631.fid"
    bpath = "#@b/dev10/0/004/689/0004689631.fid"
    FileUtils.mkdir_p([apath, bpath].map! { |x| File.dirname(x) })
    Regurgitator::Local.register port, @a
    Regurgitator::Local.register port, @b

    [ apath, bpath ].each do |path|
      File.open(path, "wb") { |fp| fp.write '.' }
    end
    assert_nil device_path_stat(uri)
  end

  def test_trylocal
    port = 1233
    env = { "REQUEST_METHOD" => "GET" }
    a = URI("http://#@addr:#{port}/dev10/0/004/689/0004689631.fid")
    b = URI("http://#@addr:#{port + 6}/dev11/0/004/689/0004689631.fid")
    assert_nil trylocal(env, [ [ a, b ] ])

    path_a = "#@tmpdir#{a.path}"
    FileUtils.mkdir_p(File.dirname(path_a))
    Regurgitator::Local.register port, @tmpdir
    File.open(path_a, "wb") { |fp| fp.write '.' }
    res = trylocal(env, [ [ a, b ] ])
    assert_kind_of Regurgitator::LocalFile, res
    assert_equal 200, res.response[0]
  end

  def test_trylocal_range
    port = 1233
    env = { "REQUEST_METHOD" => "GET", "HTTP_RANGE" => "bytes=5-15" }
    a = URI("http://#@addr:#{port}/dev10/0/004/689/0004689631.fid")
    b = URI("http://#@addr:#{port + 6}/dev11/0/004/689/0004689631.fid")
    assert_nil trylocal(env, [ [ a, b ] ])

    path_a = "#@tmpdir#{a.path}"
    FileUtils.mkdir_p(File.dirname(path_a))
    Regurgitator::Local.register port, @tmpdir
    File.open(path_a, "wb") { |fp| (0..15).each { |i| fp.write(i.to_s(16)) } }
    res = trylocal(env, [ [ a, b ] ])
    assert_kind_of Regurgitator::LocalFile, res
    assert_equal(206, res.response[0])
    body = ""
    res.response[2].each { |x| body << x }
    expect = (5..15).map { |x| x.to_s(16) }.join("")
    assert_equal expect, body
  end

  def test_trylocal_bad_range
    port = 1233
    env = { "REQUEST_METHOD" => "GET", "HTTP_RANGE" => "bytes=515-999" }
    a = URI("http://#@addr:#{port}/dev10/0/004/689/0004689631.fid")
    b = URI("http://#@addr:#{port + 6}/dev11/0/004/689/0004689631.fid")
    assert_nil trylocal(env, [ [ a, b ] ])

    path_a = "#@tmpdir#{a.path}"
    FileUtils.mkdir_p(File.dirname(path_a))
    Regurgitator::Local.register port, @tmpdir
    File.open(path_a, "wb") { |fp| (0..15).each { |i| fp.write(i.to_s(16)) } }
    res = trylocal(env, [ [ a, b ] ])
    assert_kind_of Regurgitator::LocalFile, res
    assert_equal(416, res.response[0])
  end
end
