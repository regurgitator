require 'test/unit'
require 'regurgitator'
require 'regurgitator/server_settings'

class TestServerSettings < Test::Unit::TestCase
  include Regurgitator::ServerSettings

  attr_accessor :db

  def setup
    @db = Sequel.connect 'sqlite:///'
    @db.create_table(:server_settings) do
      String :field, :size => 50, :null => false, :primary_key => true
      String :value, :size => 255
    end
    server_settings_init
  end

  def test_zones
    ds = @db[:server_settings]
    [ %w(network_zones west,east,central),
      %w(zone_west 10.1.0.0/16),
      %w(zone_central 10.2.0.0/16),
      %w(zone_east 10.3.0.0/16)
    ].each { |(field,value)| ds.insert(:field => field, :value => value) }
    assert_equal 'west', zone_for('10.1.0.8')
    assert_equal 'central', zone_for('10.2.1.8')
    assert_equal 'east', zone_for('10.3.1.8')
    assert_nil zone_for('127.0.0.1')
  end

  def test_no_zones_defined
    ds = @db[:server_settings]
    assert_nil zone_for('10.1.0.8')
    assert_nil zone_for('10.2.1.8')
    assert_nil zone_for('10.3.1.8')
    assert_nil zone_for('127.0.0.1')
  end
end
