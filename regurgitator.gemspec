# -*- encoding: binary -*-
manifest = File.exist?('.manifest') ?
  IO.readlines('.manifest').map!(&:chomp!) : `git ls-files`.split("\n")

Gem::Specification.new do |s|
  s.name = %q{regurgitator}
  s.version = (ENV['VERSION'].dup || '0.9.1')
  s.authors = ['regurgitator hackers']
  s.description = File.read('README').split("\n\n")[1]
  s.email = %q{regurgitator-public@yhbt.net}
  s.extra_rdoc_files = IO.readlines('.document').map!(&:chomp!).keep_if do |f|
    File.exist?(f)
  end
  s.files = manifest
  s.homepage = 'https://yhbt.net/regurgitator/'
  s.summary = 'regurgitator - read-only Rack endpoints for MogileFS'
  s.test_files = Dir["test/test_*.rb"]
  s.add_dependency("rack", ['~> 2.0'])
  s.add_dependency("sequel", ["~> 4.0"])
  s.add_dependency("http_spew", ['~> 0.5'])
  s.add_dependency("rpatricia", ["~> 1.0"])
  s.required_ruby_version = '>= 2.1'
  s.licenses = %w(GPL-2.0+)
end
