# -*- encoding: binary -*-
# helpers for device lookups

require_relative 'server_settings'

module Regurgitator::Device # :nodoc:
  include Regurgitator::ServerSettings

  def device_init
    server_settings_init
    @dev_cache_mtime = 0
    @dev_cache = nil
    @dev_cache_lock = Mutex.new
  end

  def self.extended(obj)
    obj.device_init
  end

  def device_uris!(opts, get_port)
    opts[:port] = get_port if get_port
    [ URI::HTTP.build(opts) ]
  end

  # Returns a hash of device info with the Integer +devid+
  # as the hash key.
  def refresh_device(force = false) # :nodoc:
    @dev_cache_lock.synchronize { refresh_device_unlocked(force) }
  end

  def refresh_device_unlocked(force) # :nodoc:
    return @dev_cache if !force && ((Regurgitator.now - @dev_cache_mtime) < 60)
    tmp = {}.compare_by_identity
    refresh_zone(force)

    sql = <<-EOS.freeze
SELECT d.devid, h.hostip, h.http_port, h.http_get_port
FROM device d
  LEFT JOIN host h ON d.hostid = h.hostid
WHERE d.status IN ('readonly','alive','drain') AND h.status = 'alive'
    EOS
    @db[sql].each do |x|
      # devices in "drain" status may hit raciness try those as a last resort
      x[:preferred] = !!(x[:d_status] =~ %r{\A(?:readonly|alive)\z})
      hostip = x[:hostip]
      port = x[:http_port] || 80
      get_port = x[:http_get_port]
      x[:ipaddr] = { :hostip => hostip }
      x[:zone] = zone_for(hostip)
      devid = x[:devid]
      o = { :path => "/dev#{devid}", :port => port, :host => hostip }
      x[:uris] = { :pri => device_uris!(o, get_port) }
      tmp[devid] = x
    end
    Regurgitator::Local.refresh_addrs!
    @dev_cache_mtime = Regurgitator.now
    @dev_cache = tmp
  end
end
