# -*- encoding: binary -*-

require_relative 'domain'
require_relative 'device'

module Regurgitator::FileInfo # :nodoc:
  include Regurgitator::Domain
  include Regurgitator::Device

  def file_info_init
    domain_init
    device_init
  end

  # returns a hash with file information and URIs for accessing the file:
  # {
  #   :fid => 3149113,
  #   :length => 17,
  #   :uris => {
  #     "east" => [ [read_write,read_only], [read_write,read_only] ],
  #     "west" => [ [read_write,read_only], [read_write,read_only] ],
  #     nil => [ [read_write,read_only], [read_write,read_only] ],
  #   }
  # }
  def file_info(env, domain, dkey, update = false)
    dmid = get_dmid(domain) or return
    devices = refresh_device(update)

    info = @db[
      'SELECT fid,length FROM file WHERE dmid = ? AND dkey = ? LIMIT 1'.freeze,
      dmid, dkey
    ].first or return
    0 == info[:length] and return info
    fid = info[:fid]
    read_uris = Hash.new { |h,k| h[k] = [] }
    drain_uris = Hash.new { |h,k| h[k] = [] }

    zone = env['regurgitator.zone']

    @db['SELECT devid FROM file_on WHERE fid = ?'.freeze, fid].each do |x|
      devinfo = devices[x[:devid]] or next
      fid >= 10_000_000_000 and next # TODO: support larger FIDs

      uris = devinfo[:uris][:pri]
      nfid = sprintf('%010u'.freeze, fid)
      /\A(\d)(\d{3})(\d{3})(?:\d{3})\z/ =~ nfid
      fid_path = "/#$1/#$2/#$3/#{nfid}.fid"
      uris = uris.map do |u|
        u = u.dup
        u.path += fid_path # must be a copy
        u
      end
      (devinfo[:preferred] ? read_uris : drain_uris)[devinfo[:zone]] << uris
    end
    uris = info[:uris] = read_uris.empty? ? drain_uris : read_uris
    (uris.empty? && ! update) ? file_info(env, domain, dkey, true) : info
  end
end
