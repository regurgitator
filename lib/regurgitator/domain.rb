# -*- encoding: binary -*-
# helpers for domain lookups
module Regurgitator::Domain # :nodoc:

  def domain_init
    @domain_lock = Mutex.new
    @domain_cache_mtime = 0
    @domain_cache = nil
  end

  # returns the +dmid+ (domain identifier/primary key) for a string +domain+
  def get_dmid(domain)
    refresh_domain[domain] || false
  end

  # We cache the list of all available domains in memory, this shouldn't
  # be too huge, though...
  #
  # Returns a hash with string namespaces as keys and dmids as values
  def refresh_domain # :nodoc:
    @domain_lock.synchronize { refresh_domain_unlocked }
  end

  def refresh_domain_unlocked # :nodoc:
    return @domain_cache if ((Regurgitator.now - @domain_cache_mtime) < 15)
    tmp = {}
    @db['SELECT dmid,namespace FROM domain'.freeze].each do |x|
      tmp[x[:namespace].freeze] = x[:dmid]
    end
    @domain_cache_mtime = Regurgitator.now
    @domain_cache = tmp
  end
end
