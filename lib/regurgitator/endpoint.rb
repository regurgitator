# -*- encoding: binary -*-
require "socket"
require_relative 'file_request'
require_relative 'file_info'
require_relative 'local'

module Regurgitator::Endpoint # :nodoc:
  include Regurgitator::FileInfo
  include Rack::Utils
  include Rack::Mime

  def endpoint_init(app, db, reproxy_key = nil)
    @app = app
    @db = db
    @reproxy_key = reproxy_key
    file_info_init
  end

  def best_addr(env)
    env.include?(@reproxy_key) ? env['REMOTE_ADDR'] : Regurgitator::Local.addr
  end

  # see nginx config examples for reproxy
  def reproxy_path(env)
    env[@reproxy_key]
  end

  # allow users to specify desired "Save As" filenames in the query string
  def filename!(env, headers, dkey)
    params = parse_query(env['QUERY_STRING'])
    cd = nil
    if fn = params['inline'] and escape(fn) == fn
      cd, dkey = 'inline', fn
    elsif fn = (params['attachment'] || params['filename']) and
          escape(fn) == fn
      cd, dkey = 'attachment', fn
    end
    headers['Content-Disposition'] = "#{cd}; filename=#{fn}" if cd
    headers['Content-Type'] = mime_type(File.extname(dkey))
  end

  def empty_file(env, dkey)
    headers = { 'Content-Length' => '0' }
    filename!(env, headers, dkey)
    [ 200, headers, [] ]
  end

  def serve_file(env, domain, dkey)
    env['regurgitator.zone'] ||= best_addr(env)
    info = file_info(env, domain, dkey) or return @app.call(env)
    serve_info(env, info, dkey)
  end

  def serve_info(env, info, dkey)
    info[:length] == 0 and return empty_file(env, dkey)
    zone_uris = info[:uris]

    zone = env['regurgitator.zone']
    begin
      uris = zone_uris.delete(zone)
      uris ||= zone_uris.delete(zone_for(Regurgitator::Local.addr))

      # no nearby zones? try all of them at once!
      if uris.nil?
        uris = []
        zone_uris.each_value { |v| uris.concat v }
        zone_uris.clear
      end
      uris or next

      status, headers, body = r = Regurgitator::FileRequest.run(env, uris)
      filename!(env, headers, dkey)
      headers['ETag'] = %("#{info[:fid]}")
      headers.delete('Connection'.freeze)

      case env["REQUEST_METHOD"]
      when "GET"
        if body.respond_to?(:uri) && path = reproxy_path(env)
          body.close if body.respond_to?(:close)
          headers['Content-Length'] = '0'
          headers['Location'] = body.uri.to_s
          headers['X-Accel-Redirect'] = path
          # yes we violate Rack::Lint here
          %w(Content-Type Last-Modified).each do |key|
            headers["X-Reproxy-#{key}"] = headers.delete(key)
          end
          return [ 302, headers, [] ]
        end
        return r
      when "HEAD"
        body.close if body.respond_to?(:close)
        return [ status, headers, [] ]
      else
        raise "BUG: Unexpected REQUEST_METHOD=#{env['REQUEST_METHOD']}"
      end
    rescue Regurgitator::NoDevices
      retry unless zone_uris.empty?
      if l = env["rack.logger"]
        l.error "no devices for #{env['REQUEST_METHOD']} #{env['PATH_INFO']}"
      end
    end until zone_uris.empty?
    @app.call(env)
  end
end
