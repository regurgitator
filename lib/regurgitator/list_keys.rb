# -*- encoding: binary -*-

require_relative 'domain'

module Regurgitator::ListKeys # :nodoc:
  include Regurgitator::Domain

  LIST_KEYS_MAX = 1000

  def list_keys(domain, opts = {})
    dmid = get_dmid(domain) or return

    after, limit, prefix = opts[:after], opts[:limit], opts[:prefix]
    limit ||= LIST_KEYS_MAX

    Integer === limit or
      raise ArgumentError, ":limit not an Integer=#{limit.inspect}"

    limit = LIST_KEYS_MAX if limit > LIST_KEYS_MAX
    sql = "SELECT fid,dkey,length,devcount,classid " \
          "FROM file WHERE dmid = #{dmid}"
    prefix and
      sql << " AND dkey LIKE /*! BINARY */ #{@db.literal(prefix+'%'.freeze)}"
    sql << " AND dkey > #{@db.literal(after)}" if after
    sql << " ORDER BY dkey LIMIT #{limit}"

    # we don't enforce collation here, give DBAs some freedom...
    ds = @db[sql]
    block_given? ? ds.each { |row| yield(row) } : ds.to_a
  end
end
