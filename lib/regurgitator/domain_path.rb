# -*- encoding: binary -*-
require_relative 'endpoint'

# matches GET and HEAD requests in the format of "/:namespace/:dkey"
# where +:dkey+ may contain multiple slashes
#
# If a client were to make a request to "http://example.com/d/foo/bar",
# this endpoint would respond with the file from the "d" domain
# with the key "foo/bar".
#
# To use as middleware:
#    require 'regurgitator'
#    db = Sequel.connect('mysql2://user@example.com/mogilefs')
#    use Regurgitator::DomainPath, db
#
# See the {domain_path.ru}[link:examples/domain_host.ru]
# example for a standalone app.
class Regurgitator::DomainPath
  include Regurgitator::Endpoint

  def call(env) # :nodoc:
    case env['REQUEST_METHOD']
    when 'GET', 'HEAD'
      env['PATH_INFO'] =~ %r{\A/([^/]+)/(.+)\z} or return @app.call(env)
      serve_file(env, $1, $2)
    else
      @app.call(env)
    end
  end

  def initialize(app, opts) # :nodoc:
    opts = { :db => opts } unless Hash === opts
    endpoint_init(app, opts[:db], opts[:reproxy_key])
  end
end
