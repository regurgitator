# -*- encoding: binary -*-
require_relative 'endpoint'

# Matches GET and HEAD requests in the format of ":domain.example.com/:dkey"
# where domain is read from the Host: header.
#
# If a client were to make a request to "http://foo.example.com/bar",
# this endpoint would respond with the file from the "foo" domain
# with the key "bar".
# To use as middleware:
#    require 'regurgitator'
#    db = Sequel.connect('mysql2://user@example.com/mogilefs')
#    use Regurgitator::DomainHost, :suffix => '.example.com', :db => db
#
# See the {domain_host.ru}[link:examples/domain_host.ru]
# example for a standalone app.

class Regurgitator::DomainHost
  include Regurgitator::Endpoint

  def initialize(app, opts) # :nodoc:
    case suffix = opts[:suffix]
    when String
      suffix = Regexp.quote(suffix)
      suffix = %r{\A(.*)#{suffix}\z}
    when Regexp
    else
      raise TypeError, ":suffix must be a String or Regexp #{suffix.inspect}"
    end
    @domain_regexp = suffix
    endpoint_init(app, opts[:db], opts[:reproxy_key])
  end

  def call(env) # :nodoc:
    case env['REQUEST_METHOD']
    when 'GET', 'HEAD'
      host = env['HTTP_HOST'] or return @app.call(env)
      domain = @domain_regexp =~ host ? $1 : host
      serve_file(env, domain, env['PATH_INFO'][1..-1])
    else
      @app.call(env)
    end
  end
end
