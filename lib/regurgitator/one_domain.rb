# -*- encoding: binary -*-

require_relative 'endpoint'

# matches GET and HEAD requests in the format of "/:dkey" from a
# single, preconfigured domain.
#
# If a client were to make a request to "http://example.com/bar",
# this endpoint would respond with the file with the key "bar"
# from the preconfigured domain.
#
# To use as middleware:
#    require 'regurgitator'
#    db = Sequel.connect('mysql2://user@example.com/mogilefs')
#    use Regurgitator::OneDomain, :domain => 'foo', :db => db
#
# See the {one_domain.ru}[link:examples/one_domain.ru]
# example for a standalone app.
class Regurgitator::OneDomain
  include Regurgitator::Endpoint

  def initialize(app, opts) # :nodoc:
    @domain = opts[:domain]
    endpoint_init(app, opts[:db], opts[:reproxy_key])
  end

  def call(env) # :nodoc:
    case env['REQUEST_METHOD']
    when 'GET', 'HEAD'
      serve_file(env, @domain, env['PATH_INFO'][1..-1])
    else
      @app.call(env)
    end
  end
end
