# -*- encoding: binary -*-
# helpers for server_settings lookups

require_relative 'local'
module Regurgitator::ServerSettings # :nodoc:

  def server_settings_init
    @zone_cache_mtime = 0
    @zone_cache = nil
    @zone_cache_lock = Mutex.new
  end

  def self.extended(obj)
    obj.server_settings_init
  end

  def refresh_zone(force = false) # :nodoc:
    @zone_cache_lock.synchronize { refresh_zone_unlocked(force) }
  end

  def refresh_zone_unlocked(force)
    return @zone_cache if !force && ((Regurgitator.now-@zone_cache_mtime) < 60)
    tmp = Patricia.new
    sql = 'SELECT value FROM server_settings WHERE field = ? LIMIT 1'.freeze

    begin
      row = @db[sql, 'network_zones'.freeze].first or return tmp
      row[:value].split(/\s*,\s*/).each do |zone|
        row = @db[sql, "zone_#{zone}"].first or next
        begin
          tmp.add(row[:value], zone)
        rescue ArgumentError
        end
      end
    ensure
      @zone_cache_mtime = Regurgitator.now
      return @zone_cache = tmp
    end
  end

  # If +skip_local+ is true, it may return +:local+ if +addr+ is the address
  # of the host this method is running on
  # Returns the zone (a String) for a given +addr+, if there is one
  # Returns +nil+ if zone information is unknown.
  def zone_for(addr, skip_local = false)
    return :local if skip_local && Regurgitator::Local.include?(addr)
    zone = refresh_zone.search_best(addr) and return zone.data # String
    nil
  end
end
