# -*- encoding: binary -*-
require 'uri'
require 'rack'
require 'thread'
require 'sequel'
require 'http_spew'
require 'rpatricia'

# The Regurgitator main module, serving as a namespace for all
# modules and classes.
#
# All modules meant for use in applications are autoload-ed,
# so just "require 'regurgitator'" in your code.
module Regurgitator

  autoload :ListKeys, 'regurgitator/list_keys'
  # Rack middlewares/apps
  autoload :DomainPath, 'regurgitator/domain_path'
  autoload :DomainHost, 'regurgitator/domain_host'
  autoload :OneDomain, 'regurgitator/one_domain'

  # used to wrap up all Regurgitator-specific extensions
  Error = Class.new(StandardError) # :nodoc:

  # raised when there are no readable devices
  NoDevices = Class.new(Error) # :nodoc:

  # raised by FileRequest when the HTTP status code
  # is outside of (200-299, 304)
  BadResponse = Class.new(Error) # :nodoc:

  def self.now # :nodoc:
    Process.clock_gettime(Process::CLOCK_MONOTONIC)
  end
end

require_relative 'regurgitator/local'
