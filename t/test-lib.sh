#!/bin/sh
. ./my-tap-lib.sh
set -e
set -u

# sometimes we rely on http_proxy to avoid wasting bandwidth with Isolate
# and multiple Ruby versions
NO_PROXY=${UNICORN_TEST_ADDR-127.0.0.1}
export NO_PROXY

RUBY="${RUBY-ruby}"
RUBY_VERSION=${RUBY_VERSION-$($RUBY -e 'puts RUBY_VERSION')}
t_pfx=$PWD/trash/$T-$RUBY_VERSION
LOCKDIR=${TMPDIR-/tmp}
export LOCKDIR
TMPDIR=$t_pfx
rm -rf $TMPDIR
mkdir -p $TMPDIR
export TMPDIR

PATH=$PWD/bin:$PATH
export PATH

test -x $PWD/bin/unused_listen || die "must be run in 't' directory"

# given a list of variable names, create temporary files and assign
# the pathnames to those variables
rtmpfiles () {
	for id in "$@"
	do
		name=$id
		_tmp=$t_pfx/$id
		eval "$id=$_tmp"

		case $name in
		*fifo)
			rm -f $_tmp
			mkfifo $_tmp
			T_RM_LIST="$T_RM_LIST $_tmp"
			;;
		*socket)
			rm -f $_tmp
			T_RM_LIST="$T_RM_LIST $_tmp"
			;;
		*)
			rm -f $_tmp
			> $_tmp
			T_OK_RM_LIST="$T_OK_RM_LIST $_tmp"
			;;
		esac
	done
}

dbgcat () {
	id=$1
	eval '_file=$'$id
	echo "==> $id <=="
	sed -e "s/^/$id:/" < $_file
}

check_stderr () {
	set +u
	_r_err=${1-${r_err}}
	set -u
	if grep -i Error $_r_err
	then
		die "Errors found in $_r_err"
	elif grep SIGKILL $_r_err
	then
		die "SIGKILL found in $_r_err"
	fi
}

setup () {
	rtmpfiles r_err r_out r_pid db f_err f_out f_pid curl_err

	random_blob_size=$(wc -c < random_blob | awk '{print $1}')
	random_blob_cksum="$(cksum < random_blob)"
	eval $(unused_listen)
	rack_file_listen=$listen
	port=$(expr $listen : '[^:]\+:\([0-9]\+\)')
	rack_file_port=$port
	rm -f $f_pid

	( rackup -s webrick \
	  -o$NO_PROXY -p$port \
	  -P$f_pid file.ru 2>> $f_err >> $f_out & ) &

	(
		cat fixtures.sql
		echo "UPDATE host SET http_get_port = $rack_file_port;"
	) | sqlite3 $db

	eval $(unused_listen)
	port=$(expr $listen : '[^:]\+:\([0-9]\+\)')
	TEST_SEQUEL=sqlite://$db
	(
		RACK_ENV=deployment
		export TEST_SEQUEL RACK_ENV
		rm -f $r_pid
		rackup -E none \
		  -s webrick -P$r_pid \
		  -o$NO_PROXY -p $port "$@" 2>> $r_err >> $r_out &
	) &

	wait_for_start $r_err
	wait_for_start $f_err
	wait
	rack_server_pid=$(cat $r_pid)
	test -n "$rack_server_pid" && kill -0 $rack_server_pid
	rack_file_pid=$(cat $f_pid)
	test -n "$rack_file_pid" && kill -0 $rack_file_pid
}

wait_for_start () {
	log_err=$1
	(
		renice 19 $$
		while ! grep 'HTTPServer#start:' $log_err
		do
			:
		done
	) >/dev/null 2>&1 &
}
