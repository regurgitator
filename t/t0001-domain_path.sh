#!/bin/sh
. ./test-lib.sh
t_plan 8 "DomainPath tests"

t_begin "setup" && {
	setup domain_path.ru
}

t_begin "retrieve empty file" && {
	test 0 -eq $(curl -sSvf 2> $curl_err http://$listen/d/blah | wc -c)
	grep 'E[tT]ag: ' $curl_err && die "ETag not expected"
	grep 'Last-Modified: ' $curl_err && die "Last-Modified not expected"
}

t_begin "retrieve missing file" && {
	sqlite3 $db <<EOF
UPDATE file SET length = $random_blob_size;
EOF
	ok=$(curl -sSvf 2> $curl_err http://$listen/d/blah || echo ok)
	test ok = "$ok"
	grep '\<404\>' $curl_err
}

t_begin "retrieve existing file" && {
	dir=$TMPDIR/dev1/0/000/000
	mkdir -p $dir
	ln random_blob $dir/0000000001.fid
	cksum="$(curl -sSvf 2> $curl_err http://$listen/d/blah | cksum)"
	test "$cksum" = "$random_blob_cksum"
	dbgcat curl_err
	grep 'E[tT]ag: "1"' $curl_err
}

t_begin "reproxy existing file" && {
	curl -sSvf -H 'X-Reproxy-Path: /reproxy' 2> $curl_err \
	  http://$listen/d/blah >/dev/null
	dbgcat curl_err
	uri=http://$rack_file_listen/dev1/0/000/000/0000000001.fid
	grep '^< X-Accel-Redirect: /reproxy' $curl_err
	grep '^< X-Reproxy-Content-Type: application/octet-stream' $curl_err
	grep "^< Location: $uri" $curl_err
	grep '^< X-Reproxy-Last-Modified: ' $curl_err
	grep '^< Content-Length: 0' $curl_err
	grep '^< Etag: "1"' $curl_err
}

t_begin "HEAD requests work" && {
	curl -vsSfI > $curl_err http://$listen/d/blah
	grep '^E[Tt]ag: "1"' $curl_err
	grep "^Content-Length: $random_blob_size" $curl_err
}

t_begin "HEAD requests work with reproxy" && {
	curl -vsSfI -H X-Reproxy-Path:/z > $curl_err http://$listen/d/blah
	grep '^E[Tt]ag: "1"' $curl_err
	grep "^Content-Length: $random_blob_size" $curl_err
	grep '^X-Accel-Redirect:' $curl_err && die "unexpected reproxy header"
	grep '^X-Reproxy' $curl_err && die "unexpected reproxy header"
	grep '^Location:' $curl_err && die "unexpected reproxy header"
}

t_begin "kill servers" && {
	kill -INT $rack_file_pid
	kill -INT $rack_server_pid
}

t_done
