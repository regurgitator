require 'regurgitator'
use Rack::ContentLength
use Rack::ContentType, 'text/plain'
o = {
  :db => Sequel.connect(ENV['TEST_SEQUEL']),
  :suffix => '.example.com',
  :reproxy_key => "HTTP_X_REPROXY_PATH",
}
run Regurgitator::DomainHost.new(lambda { |env| [ 404, [], [] ] }, o)
