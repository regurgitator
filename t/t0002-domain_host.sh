#!/bin/sh
. ./test-lib.sh
t_plan 8 "DomainHost tests"

t_begin "setup" && {
	setup domain_host.ru
}

t_begin "retrieve empty file" && {
	test 0 -eq $(curl -sSvf 2> $curl_err \
	     -H Host:d.example.com \
	     http://$listen/blah | wc -c)
	grep 'E[tT]ag: ' $curl_err && die "ETag not expected"
	grep 'Last-Modified: ' $curl_err && die "Last-Modified not expected"
}

t_begin "retrieve missing file" && {
	sqlite3 $db <<EOF
UPDATE file SET length = $random_blob_size;
EOF
	ok=$(curl -sSvf \
	     -H Host:d.example.com \
	     2> $curl_err http://$listen/blah || echo ok)
	test ok = "$ok"
	grep '\<404\>' $curl_err
}

t_begin "retrieve existing file" && {
	dir=$TMPDIR/dev1/0/000/000
	mkdir -p $dir
	ln random_blob $dir/0000000001.fid
	cksum="$(curl -sSvf 2> $curl_err \
	         -H Host:d.example.com http://$listen/blah | cksum)"
	test "$cksum" = "$random_blob_cksum"
	dbgcat curl_err
	grep 'E[tT]ag: "1"' $curl_err
}

t_begin "reproxy existing file" && {
	curl -sSvf -H 'X-Reproxy-Path: /reproxy' 2> $curl_err \
	  -H Host:d.example.com \
	  http://$listen/blah >/dev/null
	dbgcat curl_err
	uri=http://$rack_file_listen/dev1/0/000/000/0000000001.fid
	grep '^< X-Accel-Redirect: /reproxy' $curl_err
	grep '^< X-Reproxy-Content-Type: application/octet-stream' $curl_err
	grep "^< Location: $uri" $curl_err
	grep '^< X-Reproxy-Last-Modified: ' $curl_err
	grep '^< Content-Length: 0' $curl_err
	grep '^< Etag: "1"' $curl_err
}

t_begin "update host name and sleep 16s for cache refresh" && {
	sqlite3 $db <<EOF
UPDATE domain SET namespace = 'i-have-a-cname.example.org'
WHERE namespace = 'd';
EOF
}

t_begin 'retry using CNAME host name' && {
	sleep 16
	cksum="$(curl -sSvf 2> $curl_err \
	         -H Host:i-have-a-cname.example.org \
	         http://$listen/blah | cksum)"
	test "$cksum" = "$random_blob_cksum"
	dbgcat curl_err
	grep 'E[tT]ag: "1"' $curl_err
}

t_begin "kill servers" && {
	kill -INT $rack_file_pid
	kill -INT $rack_server_pid
}

t_done
