CREATE TABLE domain (
	dmid SMALLINT NOT NULL,
	namespace VARCHAR(255),
	PRIMARY KEY (dmid),
	UNIQUE (namespace)
);

CREATE TABLE file (
	fid INTEGER NOT NULL,
	dmid SMALLINT NOT NULL,
	dkey VARCHAR(255),
	length INTEGER,
	classid TINYINT NOT NULL,
	devcount TINYINT NOT NULL,
	UNIQUE (dmid, dkey),
	PRIMARY KEY (fid)
);

CREATE TABLE host (
	hostid MEDIUMINT NOT NULL PRIMARY KEY,
	status VARCHAR(5),
	http_port MEDIUMINT DEFAULT 7500,
	http_get_port MEDIUMINT,
	hostname VARCHAR(40),
	hostip VARCHAR(15),
	altip VARCHAR(15),
	altmask VARCHAR(18),
	UNIQUE (hostname),
	UNIQUE (hostip),
	UNIQUE (altip)
);

CREATE TABLE server_settings (
	field VARCHAR(50) PRIMARY KEY,
	value VARCHAR(255)
);

CREATE TABLE device (
	devid MEDIUMINT NOT NULL,
	hostid MEDIUMINT NOT NULL,
	status VARCHAR(8),
	weight MEDIUMINT DEFAULT 100,
	mb_total MEDIUMINT,
	mb_used MEDIUMINT,
	mb_asof INTEGER,
	PRIMARY KEY (devid)
);

CREATE TABLE file_on (
	fid INTEGER NOT NULL,
	devid MEDIUMINT NOT NULL,
	PRIMARY KEY (fid, devid)
);

INSERT INTO host VALUES(1,'alive',7500,NULL,'localhost','127.0.0.1',NULL,NULL);
INSERT INTO device VALUES(1,1,'alive',100,99999,0,1234567890);
INSERT INTO domain VALUES(1,'d');
INSERT INTO file VALUES(1,1,'blah',0,0,1);
INSERT INTO file_on VALUES(1,1);
