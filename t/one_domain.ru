require 'regurgitator'
use Rack::ContentLength
use Rack::ContentType, 'text/plain'
o = {
  :db => Sequel.connect(ENV['TEST_SEQUEL']),
  :domain => 'd',
  :reproxy_key => "HTTP_X_REPROXY_PATH",
}
run Regurgitator::OneDomain.new(lambda { |env| [ 404, [], [] ] }, o)
