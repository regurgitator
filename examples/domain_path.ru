require 'regurgitator'
db = Sequel.connect 'mysql2://user@example.com/mogilefs'
use Rack::ContentLength
use Rack::ContentType, 'text/plain'
run Regurgitator::DomainPath.new(lambda { |env| [ 404, [], [] ] }, db)
