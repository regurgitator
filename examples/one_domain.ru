require 'regurgitator'
db = Sequel.connect 'mysql2://user@example.com/mogilefs'
use Rack::ContentLength
use Rack::ContentType, 'text/plain'
err = lambda { |env| [ 404, [], [] ] }
run Regurgitator::OneDomain.new(err, :db => db, :domain => 'my.domain')
