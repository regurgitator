require 'regurgitator'
db = Sequel.connect 'mysql2://user@example.com/mogilefs'
use Rack::ContentLength
use Rack::ContentType, 'text/plain'
err = lambda { |env| [ 404, [], [] ] }
run Regurgitator::DomainHost.new(err, :db => db, :suffix => '.example.com')
